var express = require('express');
var router = express.Router();
var Tv = loadModule('tv');
var tv = new Tv();
var peerFlixPID = 0;

var path = require('path');
var pathDownloadedTorrents = path.resolve('downloads/torrents/');

function killProcess (pid) {
  try {
    process.kill(pid, 'SIGTERM');
  } catch (e) {

  }
}

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'TTS'});
});

router.get('/stream', function (req, res, next) {
  console.log(peerFlixPID);
  killProcess(peerFlixPID);
  res.json({stream: 'killed'});
});

router.post('/stream', function(req, res, next) {
  var magnetLink = req.body['magnet-link'];
  var port = req.body['streaming-port'] || 8888;
  var scrapLink = req.body['scrap-link'];

  console.log(magnetLink);

  if (magnetLink) {
    tv.connect(
        magnetLink,
        function (files) {
          files.forEach(function (file) {
            if (file.indexOf('.mp4') != -1) {
              tv.chooseFile(file);
              tv.stream();
            }
          });
        }
    );
    return;
  }

  if (scrapLink) {

    var request = require('request');
    var cheerio = require('cheerio');

    request(scrapLink, function (error, response, html) {
      if (!error && response.statusCode == 200) {
        var $ = cheerio.load(html);
        magnetLink = $('#magnet')[0].attribs.href;
        magnetLink = '"'+ magnetLink +'"';
        runPeerFlix();
      } else {
        console.log('error: ', error);
      }
    });

  } else {

    runPeerFlix();

  }

  function isPortInUse (port, done) {
    var net = require('net');
    var server = net.createServer();

    server.once('error', function(err) {
      if (err.code === 'EADDRINUSE') {
        done(true);
      } else {
        done(true);
      }
    });

    server.once('listening', function() {
      // close the server if listening doesn't fail
      server.close();
      done(false);
    });

    server.listen(port);
  }

  function portInUse (busy) {

    console.log('Port: '+ port + ' in use.');

    if (busy) {
      isPortInUse(parseInt(port) - 1, portInUse, torrentFile, torrentFile)
    } else {
      runPeerFlix();
    }

  }

  function runPeerFlix () {

    killProcess(peerFlixPID);

    var peerflix = require('child_process').fork('routes/peerflix.js', [magnetLink], {
      stdio: 'inherit'
    });


    process.nextTick(function () {

      peerflix.on('message', function (response) {
        console.log(response);
        peerFlixPID = response.peerFlixPID;
      });

      //peerflix.send({request: 'pid'});

    });

    res.json({ streamingLink: 'http://192.168.1.102:'+ port, magnetLink: magnetLink});

  }

});

module.exports = router;
