global.loadModule = function (name) {
    var path = require('path');
    var modulePath = path.join('..', 'app_modules', name);
    return require(modulePath);
};

var PeerflixProcess = function () {

    var self = this;
    var path = require('path');
    var customPeerflixModulePath = path.join('.', 'app_modules', 'peerflix', 'app.js');

    console.log(process.argv[2]);

    this.start = function () {
        return loadModule('spawns')(['node '+ customPeerflixModulePath +' '+ process.argv[2] +''], {
            // stdio: 'inherit'
        }).on('data', self.getPIDfromProcess);
    };

    this.pid = 0;

    this.getPIDfromProcess = function (PID) {
        var msgFlag = 'peerfix.pid', tempPID;
        if (PID.indexOf(msgFlag) != -1) {
            tempPID = PID.replace(new RegExp(msgFlag), '');
            self.pid = tempPID.replace(new RegExp(msgFlag), '').trim();
            process.send({peerFlixPID: self.pid, selfPID: process.pid});  //  this is process PID of peerflix it self
            self.getPIDfromProcess = function () {};  //  override event handler - needed only once
        }
    };

    this.kill = function () {
        process.kill(self.pid, 'SIGTERM');
    };

    //  send peerflix
    process.on('message', function (response) {
        if (response.request == 'pid') {
            process.send({peerFlixPID: peerflixProcess.pid, selfPID: process.pid});
        }
    });
};

var peerflixProcess = new PeerflixProcess();

peerflixProcess.start();