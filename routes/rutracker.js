var express = require('express');
var router = express.Router();
var RutrackerApi = loadModule('rutracker-api');
var rutracker  = new RutrackerApi();
var path = require('path');
var pathDownloadedTorrents = path.join(process.cwd(), 'downloads', 'torrents');
var Tv = loadModule('tv');
var tv = new Tv();

router.post('/credentials', function (req, res, next) {
    var responseTimeout, rutrackerLoginHandler;
    var credentials = req.body.credentials;

    rutrackerLoginHandler = function () {
        console.log('Authorisation successful');
        clearTimeout(responseTimeout);
        res.json({logged: true});
    }

    if (credentials) {
        console.log(credentials);
        rutracker.login(credentials.login, credentials.pass);
        rutracker.on('login', rutrackerLoginHandler);
        
        responseTimeout = setTimeout(function () {
            //  reset handler to make sure it wont send second response
            rutrackerLoginHandler = function () {};
            res.json({msg: 'rutracker login timeout', type: 'credentials'});
        }, 60 * 1000);
    } else {
        res.json({msg: 'No query', type: 'Empty data'});
    }
});

//  search for torrents on rutracker
router.post('/query', function(req, res, next) {

    var query = req.body.query;

    if (query) {
        rutracker.search(req.body.query, function (data) {
            res.json(data);
        });
    } else {
        res.json({msg: 'No query', type: 'Empty data'});
    }

});

router.get('/torrent/files', function (req, res, next) {

    var fs = require('fs');

    var id = req.query.id;
    var stream = req.query.stream == 'true';

    if (id && !stream) {
        //  read data from downloaded torrent
        var torrentPath = path.join(pathDownloadedTorrents, id+ '.torrent');
        var filesInTorrent = [];

        fs.exists(torrentPath, function (exists) {
            if (exists) {
                //  if torrent exists - parse it
                try {
                    filesInTorrent = tv.parseTorrent(torrentPath).files();
                } catch (e) {
                    filesInTorrent = [];
                }

                res.json(filesInTorrent);

            } else {
                //  if torrent does not exist, go to next middleware
                req.serverInside = {
                    id: id,
                    path: torrentPath
                };
                next();
            }
        });

    } else if (id && stream) {  
        //  this one is hardcoded - client needs to have the magnet-link to start streaming
        //  rutracker API extended
        //  this one is use to:
        //  1. connect with magnet link
        //  2. get available files
        rutracker.stream(id, function (magnetLink) {

            if (!tv.connected) {
                tv.connect(
                    magnetLink,
                    function (files) {
                        res.json({files: files, magentLink: magnetLink});
                    }
                );
            } else {
                tv.stop(function () {
                    tv.connect(
                        magnetLink,
                        function (files) {
                            res.json({files: files, magentLink: magnetLink});
                        }
                    );
                });
            }

        });

    } else {
        res.json({msg: 'No torrent id', type: 'Empty data'});
    }

});

//  download torrent by id from rutracker
router.get('/torrent/files', function(req, res, next) {

    var id = req.serverInside && req.serverInside.id;
    var torrentPath = req.serverInside && req.serverInside.path;

    if (id) {
        rutracker.download(id, function (data) {
            data.pipe(require('fs').createWriteStream(path.join(pathDownloadedTorrents, id +'.torrent')));
            var filesInTorrent = tv.parseTorrent(torrentPath).files();
            res.json(filesInTorrent);
        });
    } else {
        res.json({msg: 'No ID', type: 'Empty data'});
    }

});

//  stream selected file
//  does not recognize yet if file belongs to allready opened connection
//  gives no feedback
router.post('/stream', function (req, res, next) {

    var file = req.body.file;

    if (file) {
        tv.selectFile(file);
        tv.stream();
        res.json({msg: 'streaming file'});
    } else {
        res.json({msg: 'No file path', type: 'Empty data'});
    }

});

//  download torrent by id from rutracker
router.post('/download', function(req, res, next) {

    var id = req.body.id;

    if (id) {
        rutracker.download(id, function (data) {
            data.pipe(require('fs').createWriteStream(path.join(pathDownloadedTorrents, id +'.torrent')));
            res.json({download: 'done'});
        });
    } else {
        res.json({msg: 'No ID', type: 'Empty data'});
    }

});

//  stream magnet_link by id from rutracker
router.post('/stream_depreciated', function(req, res, next) {

    var id = req.body.id,
        type = req.body.type;
    console.log(type);

    if (type == 'magnet-link') {
        if (id) {
            rutracker.stream(id, function (data) {
                console.log(data);
                res.json({magnetLink: data});
            });
        } else {
            res.json({msg: 'No ID', type: 'Empty data'});
        }
    } else if (type == 'torrent-path') {
        if (id) {
            var fs = require('fs');
            var torrentPath = path.join(pathDownloadedTorrents, id +'.torrent');
            fs.exists(torrentPath, function (exists) {
               if (exists) {

                   tv.connect(
                       torrentPath,
                       function (files) {
                           files.forEach(function (file) {
                               if (file.indexOf('.mp4') != -1) {
                                   tv.chooseFile(file);
                                   tv.stream();
                               }
                           });
                       }
                   );

                   res.json({torrentPath: torrentPath});
               } else {
                   res.json({torrentPath: 'Does not exists'});
               }
            });
        } else {
            res.json({msg: 'No ID', type: 'Empty data'});
        }
    } else {
        res.json({msg: 'No ID', type: 'Empty data'});
    }

});

module.exports = router;