var address = require('network-address');
var torrentStream = require('torrent-stream');
var torrentToMagnet = require('torrent-to-magnet');  //  wont be needed in future
var parseTorrent = require('parse-torrent');
var rangeParser = require('range-parser');
var mime = require('mime');
var pump = require('pump');
var numeral = require('numeral');
var getType = mime.lookup.bind(mime);

//  TODO: reorganize code to create simple API
function Tv (configuration) {
    console.log('module tv');
    var self = this,
        engine,
        _server;

    this.link = '';

    this.connected = false;

    this.listening = false;

    this.file = '';

    this.filesData = [];

    this.files = [];

    this.config = {
        port: 8888,
        address: address()
    };

    _server = require('http').createServer;

    this.connect = function (link, done) {

        //  if connected then stop current connection
        if (this.connected) {
            this.stop();
        }

        engine = torrentStream(link || this.link);

        this.connected = true;

        if (done) {
            engine.on('ready', function () {
                self.files = engine.files;
                done( self.getFileData() );
            });
        }

    };

    //  DEPRECTIATED TODO: remove
    this.ttm = torrentToMagnet;

    this.stream = function (done) {

        if (self.file.name) {
            //console.log(self.file);
            var server = _server(serverHandler);

            //  streaming server handler
            function serverHandler (req, res) {
                //  console.log('request to streaming server: ', req.url, self.file);

                var file = self.file;
                var range = req.headers.range;

                range = range && rangeParser(file.length, range)[0];
                res.setHeader('Content-Type', getType(file.name));
                res.setHeader('Accept-Ranges', 'bytes');
                res.setHeader('transferMode.dlna.org', 'Streaming');
                res.setHeader('contentFeatures.dlna.org', 'DLNA.ORG_OP=01;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=017000 00000000000000000000000000');
                if (!range) {
                    //  first connection, create stream
                    res.setHeader('Content-Length', file.length);
                    if (req.method === 'HEAD') return res.end();
                    pump(file.createReadStream(), res);
                    return;
                }

                res.statusCode = 206;
                res.setHeader('Content-Length', range.end - range.start + 1);
                res.setHeader('Content-Range', 'bytes ' + range.start + '-' + range.end + '/' + file.length);
                if (req.method === 'HEAD') return res.end();
                //  next connections, generate stream based on range from headers
                pump(file.createReadStream({
                    start: range.start,
                    end: range.end
                }), res);

            }

            //  start server listening
            //  once started is ON, only torrent connection is changing
            if (!self.listening) {
                server.listen(self.config.port, self.config.address, function () {
                    self.listening = true;
                    console.log('streaming on '+ self.config.address +':'+ self.config.port);
                });
            }

        } else {
            console.log('No files');
        }

        return engine;

    };

    this.getFileData = function () {
        self.files.forEach(function (file) {
            self.filesData.push( {name: file.name, size: numeral(file.length).format('0,0 b')} );
        });
        return self.filesData;
    };

    this.selectFile = function (fileName) {
        self.files.forEach(function (file) {
            if (fileName == file.name) {
                self.file = file;
            }
        });
    };

    //  stop torrent engine and clear all attached data
    this.stop = function (done) {

        engine.destroy(destroyHandler);

        this.connected = false;
        this.files = [];
        this.filesData = [];
        
        function destroyHandler () {
            console.log('stop');
            if (done) {
                done();
            }
        }

    };
    //  parse torrent - TODO: create separate module with extended torrent parsing
    this.parseTorrent = function (torrentPath) {
        console.log('tv parseTorrent: '+ torrentPath);
        if (torrentPath) {
            var fs = require('fs');
            var torrent = {};
            var dataTorrent;

            try {  //  does not allways work
                dataTorrent = parseTorrent(fs.readFileSync(torrentPath));

                torrent.files = dataTorrent.files;
                torrent.name = dataTorrent.name;
                torrent.created = dataTorrent.created;
                torrent.infoHash = dataTorrent.infoHash;
                torrent.magnetLink = parseTorrent.toMagnetURI(dataTorrent);
            } catch (e) {

            }

            return {
                all: function () {
                    return torrent;
                },
                files: function () {
                    return torrent.files;
                },
                magnet: function () {
                    return torrent.magnetLink;
                }
            };
        } else {
            return {
                toMagnet: function (url, cb) {

                    parseTorrent.remote(url, function (err, parsedTorrent) {
                        if (err) {
                            cb( {err: err} );
                        } else {
                            cb( parseTorrent.toMagnetURI(parsedTorrent) );
                        }
                    });

                }
            };
        }
    };

    //  DEPRECTIATED TODO: remove
    this.test = function () {

        console.log('test');
        var link = '5285475.torrent';
        //  TODO: analize torrentMagnet code
        torrentToMagnet('https://otorrents.com/download/[otorrents.com]star-trek-beyond-2016-720p.torrent', {}, function (err, uri) {
            console.log(uri);
            self.connect(uri, function (files) {
                console.log(files);
                files.forEach(function (file) {
                    if (file.indexOf('.mp4') != -1) {
                        self.selectFile(file);
                    }
                });
                self.stream();
            });
        });
    }
    
}
module.exports = Tv;