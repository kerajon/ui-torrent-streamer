(function (window, angular) {
    'use strict';

    var TTS = angular.module('TorrentToStream', []);

    TTS.service('TTS_api', function ($q, $http) {

        var api = {};

        api.getMovie = function (data) {

            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: '/stream',
                data: data
            }).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });

            return deferred.promise;

        };

        api.streamON = function (link) {

            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: '/stream',
                data: { "magnet-link": link }
            }).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });

            return deferred.promise;

        };

        api.streamOFF = function () {

            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: '/stream'
            }).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });

            return deferred.promise;

        };

        //  rutracker - search
        api.query = function (query) {

            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: '/api/rutracker/query',
                data: { query: query }
            }).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });

            return deferred.promise;

        };

        //  rutracker - download
        api.download = function (id) {

            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: '/api/rutracker/download',
                data: { id: id }
            }).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });

            return deferred.promise;

        };

        //  rutracker - download
        api.getFilesByID = function (id) {

            var deferred = $q.defer();
            console.log(id)

            $http({
                method: 'GET',
                url: '/api/rutracker/torrent/files',
                params: { id: id , stream: 'true'}
            }).then(function (data) {
                deferred.resolve(data.data);
            }, function (err) {
                deferred.reject(err);
            });

            return deferred.promise;

        };

        //  rutracker - download
        api.stream = function (id) {

            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: '/api/rutracker/stream',
                data: { id: id, type: 'torrent-path' }
            }).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });

            return deferred.promise;

        };

        //  rutracker - download
        api.streamFile = function (file) {

            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: '/api/rutracker/stream',
                data: { file: file }
            }).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });

            return deferred.promise;

        };

        api.saveCredentials = function (credentials) {

            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: '/api/rutracker/credentials',
                data: { credentials: credentials }
            }).then(function (data) {
                deferred.resolve(data.data);
            }, function (err) {
                deferred.reject(err);
            });

            return deferred.promise;

        };

        return api;
        
    });
    
    TTS.service('TTS_Player', function () {

        var videoElement = document.getElementById('video');

        // videoElement.addEventListener('error', function (e) {
        //     console.log('ERROR: --> ', e);
        //     setTimeout(function () {
        //         videoElement.load();
        //     }, 5000 );
        // }, false);

        var Player = function () {

            this.playing = false;

            this.play = function (url) {
                videoElement.src = url;
                videoElement.type = 'video/mp4';
                this.playing = true;
                videoElement.play();
            };

        };

        return Player;
        
    });

    TTS.service('TTS_Decorate', function () {

        var Decorate = function () {

            this.temp = 'Container for temporary data';

        };

        Decorate.prototype.torrent = function (torrents) {

            var self = this;

            if (angular.isArray(torrents)) {

                self.temp = torrents.map(function (torrent) {

                    var decorated = {};
                    var keysToDecorate = ['id', 'leechs', 'seeds', 'size', 'category', 'title', 'url'];

                    Object.keys(torrent).forEach(function (key) {
                        if (keysToDecorate.indexOf(key) != -1) {
                            decorated[key] = torrent[key];
                        }
                    });
                    decorated.files = [];
                    decorated.streaming = false;
                    decorated.loading = false;
                    decorated.expanded = false;
                    decorated.type = 'folder_open';

                    return decorated;

                });

                return self.temp;

            } else {
                console.log('Decorate torrent data is not a Array. Type: '+ typeof data);
            }

        };

        return new Decorate();

    });
    
    TTS.controller('main', function ($scope, TTS_api, TTS_Player, TTS_Decorate) {
        $scope.streamLink = '';
        var player = new TTS_Player();
        $scope.credentials = {
            login: '',
            pass: '',
            logged: false
        };
        
        //  DEPRECTIATED TODO: remove
        this.getLink = function () {
            TTS_api.getMovie({
                "magnet-link": $scope.magnetLink,
                "streaming-port": $scope.streamingPort,
                "scrap-link": $scope.scrapLink
            }).then(function (respond) {

                player.play(respond.data.streamingLink);

            }, function (err) {
                console.log(err);
            });
        };

        this.search = function () {
            TTS_api.query(
                $scope.query
            ).then(function (respond) {
                $scope.data = TTS_Decorate.torrent(respond.data);
                console.log($scope.data);
            }, function (err) {
                console.log('Failed on search. There was error: ', err);
            });
        };

        this.download = function (id) {
            TTS_api.download(
                id
            ).then(function (respond) {
                console.log(respond);
            }, function (err) {
                console.log('Failed on download. There was error: ', err);
            });
        };

        this.getFilesFromTorrent = function (torrent) {
            torrent.loading = true;
            TTS_api.getFilesByID(
                torrent.id
            ).then(function (data) {
                console.log(data);
                data.streaming = false;
                torrent.loading = false;
                //  decorate files in torrents
                torrent.files = data.files.map(function (file) {
                    file.loading = false;
                    file.streaming = false;   
                    return file;                 
                });
            }, function (err) {
                torrent.loading = false;                
                console.log('Failed on get files from torrent. There was error: ', err);
            });
        };

        this.streamFile = function (file) {
            //  reset all files = streaming false
            file.parent.forEach(function (file) {
                file.streaming = false;
                file.loading = false;
            });
            //  set this file = streaming true
            file.streaming = true;
            file.loading = true;

            TTS_api.streamFile(
                file.name
            ).then(function (respond) {
                console.log(respond);
                file.loading = false;                
            }, function (err) {
                console.log(err);
                file.loading = false;                
            });
        };

        this.signIn = function () {

            var cred = $scope.credentials;

            console.log(cred);

            if (cred.login && cred.pass) {
                TTS_api.saveCredentials(
                    cred
                ).then(function (respond) {
                    console.log(respond);
                    if (respond.logged) {
                        $scope.openLogin = false;
                        cred.logged = true;
                    }                
                }, function (err) {
                    console.log(err);
                    cred.logged = false;               
                });
            }

        };

        //  DEPRECTIATED TODO: remove
        this.stream = function (id) {
            TTS_api.stream(
                id
            ).then(function (respond) {
                var torrentPath = respond.data.torrentPath;
                console.log(torrentPath);
                TTS_api.streamON(torrentPath);
            }, function (err) {
                console.log(err);
            });
        };

        //  DEPRECTIATED TODO: remove
        this.killStream = function () {
            TTS_api.streamOFF();
        };

    });

})(window, angular);