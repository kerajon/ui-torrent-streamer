## This is Proof of Concept.

From here type:
```
$ npm start
```

Server listens on local address at port `3000`

Streamin server listens on network address at port `8888`

`TODO:` streamer server to separate module
`TODO:` streamer module as separate child process so it can be killed
`TODO:` streamer optimalization [api, server]
`TODO:` streamer will have own events
`TODO:` client: round red button - streamer notification
`TOTO:` client: ui-router [setting, login, search, ...]
`TODO:` client: loading indicators
`TODO:` client: file type recognition for displaying icons
`TODO:` extend rutracker api
`TODO:` client: settings [application, user]
`TODO:` in general code has to be reorganize to new application
`TODO:` remove from app_modules/ [peerflix, spawns] these modules were reorganized and change structure

install dependencies in app_modules
there is no automatic task tool yet